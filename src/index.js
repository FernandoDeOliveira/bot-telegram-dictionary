const express = require('express');
const app = express();
const path = require('path');
// const router = express.Router();
const Telegraf = require('telegraf')

const dict = require('./dict_api')
const utils = require('./utils')


const bot = new Telegraf(process.env.BOT_TOKEN)

bot.start((ctx) => ctx.reply(utils.welcomeText))

bot.command('significado', async (ctx) => {
    const word = utils.getWord(ctx.update.message.text)
    const wordDefinitions = await dict.findMeaning(word)
    
    await ctx.replyWithMarkdown(`${word}\n${wordDefinitions.join('\n')}`)

    
})

bot.launch()


const PORT = process.env.PORT || 3000;
const HOST = '0.0.0.0';

app.use(express.static(__dirname + '/dist/'));

app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'))
})



// router.get('/', function(req, res){
//     file_path = path.join(__dirname+"/index.html");
//     res.sendFile(file_path);
// });

// app.use('/', router);

app.listen(PORT, HOST);

 
