const axios = require('axios')
const utils = require('./utils')


const fetchInDict = async (word) => {
    const url = `https://api.dicionario-aberto.net/word/${word}`
    const result = await axios.get(url)
    return result.data
}

const findMeaning = async (word) => {
    const content = await fetchInDict(word)
    const definitions = utils.cleanContent(content)
    return definitions
}

module.exports = {
    findMeaning: findMeaning,
}


