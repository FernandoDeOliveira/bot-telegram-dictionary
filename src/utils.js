const welcomeText = 'Bem vindo ao dicionáiro tertúlia\n' +
    'Para utilizá-lo é simples, basta escrever "/significado <palavra>"' +
    'Se a palavra for localizada, será retornado o significado dela.\n' +
    'Faça bom proveito'


function getWord(phrase) {
    return phrase.split(" ")[1]
}

function cleanContent(response) {
    var full_string = ''
    response.forEach(function(def){
        full_string += def.xml
    })
    const definitions = getDefinitions(full_string)
    return definitions
}

function getDefinitions(full_string) {
    const upper_indexs = getIndicesOf('<def>', full_string)
    const lower_indexs = getIndicesOf('<\/def>', full_string)
    const intervals = upper_indexs.map(function (u, i) {
        return [u, lower_indexs[i]];
    })
    var definitions = []

    intervals.forEach(function (interval) {
        var def = full_string.substring(interval[0], interval[1])
        definitions.push(def)
    })

    return definitions

}


function getIndicesOf(pattern, str) {
    var patternLen = pattern.length;
    if (patternLen == 0) {
        return [];
    }
    var startIndex = 0, index, indices = [];
    while ((index = str.indexOf(pattern, startIndex)) > -1) {
        if (pattern == '<def>'){
            indices.push(index + patternLen)
        }
        else{
        indices.push(index);
        }
        startIndex = index + patternLen;
    }
    return indices;
}


module.exports = {
    getWord: getWord,
    cleanContent: cleanContent,
    welcomeText: welcomeText,

}