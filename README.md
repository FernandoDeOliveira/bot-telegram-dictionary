# **Dicionário Tertúlia**
Projeto utiliza [Node](https://nodejs.org/en/) e [Docker](https://www.docker.com/)

## Clonar projeto para desenvolvimento

User o comando para clonar o projeto em seu computador

```bash

$ git clone https://gitlab.com/FernandoDeOliveira/bot-telegram-dictionary.git

```

## Criando bot

### BotFather

- Procure `BotFather` no telegram
- Digite `/newbot` e siga as instruções
- Crie um arquivo chamdo `.env` na raiz do projeto e ponha o token do seu bot da seguinte maniera:
```
BOT_TOKEN=<SEU TOKEN AQUI>
```
## Docker 
- Instale docker e docker-compose conforme a [documentação](https://docs.docker.com/engine/install/ubuntu/)

## Run
Após instalação do execute o comando
```bash
docker-compose up
```

Pronto, basta procurar por seu bot de dicionário no telegram e utilizá-lo


### Este repositório
Para ver o funcionamento deste repositório em ação acesse o site [Dicionário Tertũlia](https://tertulia-dict-bot.herokuapp.com/)
